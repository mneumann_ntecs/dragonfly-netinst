# dragonfly-netinst

A disk image to install DragonFly BSD remotely via `ssh`. Especially suited for
installations on remote servers without physical access nor remote KVM.
Requires ability to boot into a rescue system.

## Why the DragonFly installer is not working for me

* Remote booting the DragonFly installation ISO via KVM console can be pretty slow.
  This also requires that the ISO is present on a Windows share. It did this once or
  twice and it worked, but took quite long time and is a hassle to setup.

* If remote booting an installation ISO is no option, you need physical access to the machine.
  Either a USB stick to be plugged in or a CD-ROM inserted. As my box is in a data center, both
  is not an option. 

* Another solution I tried and [described in my blog post][1] requires two hard disks. A modified
  USB installation image is copied to one disk and when booted from, provides access to
  the DragonFly installer via ssh.
  But you need two disks as the installer image cannot "overwrite" itself on the disk it is booted
  from. This is basically what `dragonfly-netinst` is all about, as it can overwrite itself. This 
  is accomplished by using a rescue image that the boot loader completely loads into memory. 
  
## My use case

* I have a dedicated server where only Linux/FreeBSD is officially supported.

* I can boot up a Linux or FreeBSD rescue system on that server and gain access
  via `ssh`.

* From the rescue system, I download a prepared `netinst.img` and `dd` it to
  the servers hard disk. I have prepared the `netinst.img` before, so that it
  contains an `authorized_keys` file. See [Preparing netinst on Linux](preparing-netinst-on-linux)
  section below.

* Now I reboot the server, and after a while, I should be able to `ssh` into
  the server as `root`.

* From there on, I can continue with the installation. Note that in contrast to
  the official DragonFly live-image installer, with `netinst` I can install on
  the same disk as `netinst` was copied to.

## How it works

This is based on the DragonFly rescue system (initrd). The kernel mounts a memory file system that contains a tiny rescue system. The rescue system automatically starts sshd on all network interfaces. To secure the login, it mounts a MS-DOS partition that contains the `authorized_keys` file.

Once the user gains remote access, he or she can proceed with the installation.

It is even possible to copy the disk image onto a hard-disk, boot from the hard-disk into the rescue system, and then install DragonFly on the same hard-disk. This is useful, for instance, to install DragonFly on a dedicated server, where a DragonFly installation is not natively supported but where you can boot into a Linux rescue system. From the Linux rescue system you can then download the `dragonfly-netinst` image, `dd` it to the hard-disk and reboot.

## Partition layout

* UFS partition that contains `kernel` and the various files required for booting as well as the `initrd.img` rescue image.

* MS-DOS partition that contains configuration data. By using a MS-DOS partition, configuration from nearly every system is made really easy. It contains at least the `authorized_keys` file required for remote authentication.

## Preparing netinst on Linux

Determine the sector size and on which sector the FAT32 `Config` partition begins:

    $ fdisk -l netinst.img 
    Disk netinst.img: 384 MiB, 402653184 bytes, 786432 sectors
    Units: sectors of 1 * 512 = 512 bytes
    Sector size (logical/physical): 512 bytes / 512 bytes
    I/O size (minimum/optimal): 512 bytes / 512 bytes
    Disklabel type: dos
    Disk identifier: 0x00000000

    Device       Boot  Start    End Sectors  Size Id Type
    netinst.img1          64 133119  133056   65M ef EFI (FAT-12/16/32)
    netinst.img2      133184 266239  133056   65M  c W95 FAT32 (LBA)
    netinst.img3 *    266304 784383  518080  253M a5 FreeBSD

In this case, the sector size is `512` and the config partition starts at
sector `133184`.  With this information we can mount the `Config` partition and
make the neccessary changes:

    $ sudo mkdir /mnt/netinst_config
    $ sudo mount -t auto -o loop,offset=$((133184*512)) netinst.img /mnt/netinst_config
    
Add your public ssh key to `/mnt/netinst_config/Config/authorized_keys.txt` and
then unmount again:

    $ sudo umount /mnt/netinst_config

[1]: https://www.ntecs.de/posts/installing-dragonfly-on-a-dedicated-hetzner-server/
