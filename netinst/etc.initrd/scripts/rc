#!/bin/sh

export PATH=/bin:/sbin
export HOME=/var/home
export TERM=cons25w

# network devices to configure DHCP
NETIF="em0 emx0 re0 bge0"

rescue_shell() {
	echo "Starting the recovery shell ..."
	cat /etc/motd
	exec sh
	exit 0
}

find_netinst_disk() {
	for _dev in /dev/da? /dev/nvme? /dev/ad?; do
		if [ -e ${_dev} -a -e ${_dev}s2 -a -e ${_dev}s3 ]; then
			local label=$(disklabel ${_dev}s3 | grep -c "label: DragonFly_NetInst")
			if [ "${label}" = "1" ]; then
				echo ${_dev}
				return 0
			fi
		fi
	done
	return 1
}

mount_config_partition() {
	local mnt=$1
	local config_part=$(find_netinst_disk)s2

	if [ -e ${config_part} ]; then
		mount_msdos -o rdonly ${config_part} ${mnt}
	fi
}

wait_for_network() {
	while true; do
		echo "Ping google.de"
		ping -o -t 1 google.de && break
		sleep 1
	done
}

main() {
	echo "Setting up /var directories ..."
	mount_tmpfs tmpfs /var
	mkdir /var/db /var/empty /var/home /var/run /var/tmp

	echo "Make /etc writable"
	cpdup /etc /tmp/etc
	mount_null /tmp/etc /etc

	echo "Make /root writable"
	cpdup /root /tmp/root
	mount_null /tmp/root /root

	echo "Configure network"
	for _netif in ${NETIF}; do
		dhclient -w ${_netif} 
	done
	wait_for_network

	echo "Generate sshd host keys"
	ssh-keygen -A

	echo "Install authorized_keys from Config partition"
	mkdir /tmp/Config
	mount_config_partition /tmp/Config
	if [ -f /tmp/Config/authorized_keys.txt ]; then
		tr -d "\r" < /tmp/Config/authorized_keys.txt >> /root/.ssh/authorized_keys
	fi
	umount /tmp/Config

	echo "Starting sshd"
	/bin/sshd

	echo "Executing additional rc scripts ..."
	for rcs in /etc/rc.*; do
		if [ -x "$rcs" ]; then
			. $rcs
		fi
	done

	echo "Starting rescue shell"
	rescue_shell
}

main
