#!/bin/sh

. ./helper.sub

# --------------------------------------------------
# Constants
# --------------------------------------------------

# Partition types for fdisk
PARTITION_TYPE_DFLY=165
PARTITION_TYPE_EFI=239
PARTITION_TYPE_FAT32=12

RESCUE_DIRS="rescue rescue.libcrypto oinit"

OBJCOPY_FLAGS=--strip-debug

# --------------------------------------------------
# Functions
# --------------------------------------------------

partition_disk_with_three_partitions() {
	local disk=$1
	local mbr_file=$2

	fdisk -b ${mbr_file} -IB ${disk} 2>/dev/null >/dev/null &&
	    log "Set up legacy MBR" ||
	    error 1 "Failed to set up legacy MBR on ${disk}"

	local geometry=$(fdisk_chs_geometry ${disk})
	local total_size=$(fdisk_size_of_partition_1 ${disk})

	# partition 1 (EFI) will get 64 MiB
	local size_part1=133120
	local start_part1=63

	# partition 2 (FAT32) will get 64 MiB
	local size_part2=133120
	#local size_part2=532480
	local start_part2=$(bc -e "(${start_part1} + ${size_part1})" -equit)

	# partition 3 (DFly) will use the remaining space
	local size_part3=$(bc -e "(${total_size} - ${size_part2} - ${start_part2} - 1)" -equit)
	local start_part3=$(bc -e "(${start_part2} + ${size_part2})" -equit)
	
	cat <<-EOF | fdisk -iv -f - ${disk}
	g ${geometry}
	p 1 ${PARTITION_TYPE_EFI} ${start_part1} ${size_part1}
	p 2 ${PARTITION_TYPE_FAT32} ${start_part2} ${size_part2}
	p 3 ${PARTITION_TYPE_DFLY} ${start_part3} ${size_part3}
	a 3
EOF
	[ "$?" = "0" ] &&
	    log "Written partition to disk ${disk}" ||
	    error 1 "Failed to partition disk ${disk}"

	fdisk -s ${disk}
}

setup_efi_partition() {
	local device=$1
	local bootdir=$2
	local mnt=$(mktemp -d)

	newfs_msdos -F 32 -c 2 -L EFI -m 0xf8 ${device}
	mount_msdos ${device} ${mnt}

	mkdir -p ${mnt}/EFI/BOOT
	cp ${bootdir}/boot1.efi ${mnt}/EFI/BOOT/BOOTX64.EFI
	umount ${mnt}
	rmdir ${mnt}
}

setup_fat32_partition() {
	local device=$1
	local mnt=$(mktemp -d)

	newfs_msdos -F 32 -c 2 -L CONFIG ${device}
	mount_msdos ${device} ${mnt}

	touch ${mnt}/ReadMe.txt
	mkdir -p ${mnt}/Config
	touch ${mnt}/Config/authorized_keys.txt

	umount ${mnt}
	rmdir ${mnt}
}

setup_dfly_partition() {
	local device=$1
	local bootdir=$2
	local kerneldir=$3
	local initrd_image_gz=$4

	local mnt=$(mktemp -d)
	local label=$(mktemp)

	log "write standard disklabel"
	disklabel -w -r ${device} auto

	log "read disklabel back"
	disklabel -r ${device} > ${label}

	log "set disklabel name"
	echo "label: DragonFly_NetInst" >> ${label}

	log "add slice parition"
	echo "a: * * 4.2BSD" >> ${label}

	log "write modified disklabel back"
	disklabel -R -r ${device} ${label}

	rm ${label}

	disklabel -B -b ${bootdir}/boot1_64 -s ${bootdir}/boot2_64 ${device}

	newfs ${device}a

	mount ${device}a ${mnt}

	# ---------------------------------
	log "install boot loader"
	# ---------------------------------

	mkdir -p ${mnt}/boot

	# config files
	cpdup -o -u ./boot ${mnt}/boot

	# boot loader
	cp ${bootdir}/loader ${bootdir}/loader.efi ${bootdir}/loader.help ${mnt}/boot

	# ---------------------------------
	log "install kernel and modules"
	# ---------------------------------

	mkdir -p ${mnt}/boot/kernel

	for _k in kernel xhci.ko ehci.ko; do
		objcopy ${OBJCOPY_FLAGS} \
			${kerneldir}/${_k} ${mnt}/boot/kernel/${_k} ||
			error 1 "Failed to copy"
	done

	# ---------------------------------
	log "install initrd image"
	# ---------------------------------

	cp ${initrd_image_gz} ${mnt}/boot/kernel/initrd.img.gz ||
		error 1 "Failed to copy"

	umount ${mnt}
	rmdir ${mnt}
}

build_rescue() {
	local src_root=$1

	for _dir in ${RESCUE_DIRS}; do
		make -C ${_dir} SRC_ROOT=${src_root} || error 
	done
}

stage_initrd() {
	local src_root=$1
	local stage_dir=$2
	local prepopulate_with_authorized_keys_file=$3

	# --------------------------------------------------
	# Make hierarchy
	# --------------------------------------------------

	mkdir -p ${stage_dir}/new_root ||
	    error 1 "Failed to mkdir ${stage_dir}/new_root"

	# Symlink 'sbin' to 'bin'
	ln -sf bin ${stage_dir}/sbin

	# Symlink 'tmp' to 'var/tmp', as '/var' will be mounted with
	# tmpfs, saving a second tmpfs been mounted on '/tmp'.
	ln -sf var/tmp ${stage_dir}/tmp
	for _dir in bin dev etc mnt var; do
		[ ! -d "${stage_dir}/${_dir}" ] &&
		    mkdir -p ${stage_dir}/${_dir}
	done
	echo "Created directory structure"

	# --------------------------------------------------
	# Stage rescue dirs
	# --------------------------------------------------

	for _dir in ${RESCUE_DIRS}; do
		env LC_ALL=C make -C ${_dir} -m ${src_root}/share/mk BINDIR=${stage_dir}/bin DESTDIR="" install ||
		error 1 "Failed to stage rescue ${_dir}"
	done

	# --------------------------------------------------
	# Copy etc.initrd
	# --------------------------------------------------

	cpdup -o -u ./etc.initrd/files ${stage_dir}/etc
	(cd ./etc.initrd/files &&
		find . -type f | xargs -I % chmod 444 ${stage_dir}/etc/%)

	cpdup -o -u ./etc.initrd/scripts ${stage_dir}/etc
	(cd ./etc.initrd/scripts &&
		find . -type f | xargs -I % chmod 555 ${stage_dir}/etc/%)

	# --------------------------------------------------
	# Init other files
	# --------------------------------------------------

	pwd_mkdb -d ${stage_dir}/etc ${stage_dir}/etc/master.passwd

	# --------------------------------------------------
	# Setup /root/.ssh
	# --------------------------------------------------

	mkdir -p ${stage_dir}/root
	mkdir -p ${stage_dir}/root/.ssh
	touch ${stage_dir}/root/.ssh/authorized_keys
	if [ -f "${prepopulate_with_authorized_keys_file}" ]; then
		cat "${prepopulate_with_authorized_keys_file}" >> ${stage_dir}/root/.ssh/authorized_keys
	fi

	chmod 644 ${stage_dir}/root/.ssh/authorized_keys
}

create_initrd_image() {
	local image=$1
	local image_size_in_mib=$2
	local stage_dir=$3

	[ -e ${image} ] &&
		error 1 "Image file ${image} already exists"

	local mnt=$(mktemp -d)

	truncate -s ${image_size_in_mib}M ${image}

	local vn_dev=$(create_vn ${image} ${image_size_in_mib})

	local device=/dev/${vn_dev}s0

	newfs -i 131072 -m 0 ${device} &&
	    echo "Formatted initrd image with UFS" ||
	    error 1 "Failed to format the initrd image"
	mount_ufs ${device} ${mnt} &&
	    echo "Mounted initrd image on ${mnt}" ||
	    error 1 "Failed to mount initrd image on ${mnt}"

	cpdup -u -o ${stage_dir}/ ${mnt}/

	# --------------------------------------------------
	# Unmount
	# --------------------------------------------------

	umount ${device} &&
	    echo "Unmounted initrd image" ||
	    error 1 "Failed to umount initrd image"

	rmdir ${mnt}

	destroy_vn ${vn_dev}
}


main() {
	local src_root=/home/mneumann/dragonfly-src
	local initrd_image=initrd.img
	local disk_image=/tmp/netinst.img
	local disk_size_in_mib=384
	local bootdir=/boot
	local kerneldir=/boot/kernel
	local prepopulate_with_authorized_keys_file=""

	build_rescue ${src_root} ||
		error 1 "Failed to build rescue system"

	local stage_dir=$(mktemp -d)

	echo ${stage_dir}

	stage_initrd ${src_root} ${stage_dir} ${prepopulate_with_authorized_keys_file} ||
		error 2 "Failed to stage initrd"

	sz=$(calc_size ${stage_dir})
	echo "----------------------------------------------------"
	echo $sz
	echo "----------------------------------------------------"

	# XXX: Use calculated size
	create_initrd_image ${initrd_image} 12 ${stage_dir}
	rm -rf ${stage_dir}

	gzip -9 ${initrd_image}

	rm -f ${disk_image} ${disk_image}.gz
	truncate -s ${disk_size_in_mib}M ${disk_image}
	vn_dev=$(create_vn ${disk_image} ${disk_size_in_mib})
	partition_disk_with_three_partitions ${vn_dev} ${bootdir}/mbr

	setup_efi_partition /dev/${vn_dev}s1 ${bootdir}
	setup_fat32_partition /dev/${vn_dev}s2
	setup_dfly_partition /dev/${vn_dev}s3 ${bootdir} ${kerneldir} ${initrd_image}.gz

	destroy_vn ${vn_dev}

	#dd if=${disk_image} of=/dev/da8 bs=1m
	gzip -9 ${disk_image}

}

main

# make DESTKERNDIR=/tmp/.. installkernel
