#
# Configuration
#
# PLEASE EDIT!!!
#

DISK="" # e.g. "da0"
NETIF="" # e.g. "re0"
HOSTNAME="" # your hostname
SWAPSIZE=32g # 2*RAM
MIRROR=http://ftp.halifax.rwth-aachen.de/dragonflybsd/iso-images
LIVECD_IMG=dfly-x86_64-5.6.2_REL.img.bz2
EXPECTED_SHA1=3b4d42f33ad29f5533b6cf8bc3017afec8b76d2d

################################################################

expect() {
	if [ -z "${1}" ]; then
		echo "ERROR: Missing ${2}"
		exit 1
	fi
}

expect "${DISK}" DISK
expect "${NETIF}" NETIF
expect "${HOSTNAME}" HOSTNAME
expect "${SWAPSIZE}" SWAPSIZE
expect "${MIRROR}" MIRROR
expect "${LIVECD_IMG}" LIVECD_IMG
expect "${EXPECTED_SHA1}" EXPECTED_SHA1


NETINST_BASE=/tmp/netinst
BASE=${NETINST_BASE}/live-cd
DEST=${NETINST_BASE}/root_mnt
BOOTDIR=${BASE}/boot

fetch_livecd() {
	if [ ! -e ${NETINST_BASE}/livecd.img ]; then
		fetch --no-verify-peer -o - ${MIRROR}/dfly-x86_64-5.6.2_REL.img.bz2 | sha1 -P 2>${NETINST_BASE}/checksum | bzcat > ${NETINST_BASE}/livecd.img
	else
		echo "livecd.img already exists. Cannot check SHA1!!!"
		exit 1
	fi
	local sha1=`cat ${NETINST_BASE}/checksum`
	if [ "${sha1}" != "${EXPECTED_SHA1}" ]; then
		rm livecd.img
		echo "INVALID CHECKSUM!!!!"
		echo "Given: ${sha1}. Expected: ${EXPECTED_SHA1}" 
		exit 1
	fi
}

mount_livecd() {
	if [ ! -e ${NETINST_BASE}/livecd.img ]; then
		echo "ERROR: livecd.img does not exist"
		exit 1
	fi

	mkdir ${BASE}
	VNDEV=`vnconfig -e vn ${NETINST_BASE}/livecd.img`
	mount /dev/${VNDEV}s2a ${BASE} || exit 1

	if [ ! -e ${BASE}/README ]; then
		echo "Failed to mount livecd"
		exit 1
	fi
}

unmount_livecd() {
	vnconfig -u ${VNDEV} 
	umount ${BASE}
}

partition_disk() {
	dd if=/dev/zero of=/dev/${DISK} bs=32k count=16
	fdisk -b ${BOOTDIR}/mbr -IB ${DISK}
	disklabel64 -r -w ${DISK}s1
	disklabel64 -b ${BOOTDIR}/boot1_64 -s ${BOOTDIR}/boot2_64 -B ${DISK}s1
	local label=${NETINST_BASE}/label
	disklabel64 ${DISK}s1 > ${label}

	cat >> ${label} <<EOF
	  a: 1g 0 4.2BSD
	  b: ${SWAPSIZE} * swap
	  d: * * HAMMER2
EOF

	disklabel64 -R ${DISK}s1 ${label}
	rm ${label}
}

create_fs() {
	# Create file systems
	newfs /dev/${DISK}s1a
	newfs_hammer2 -f -L ROOT /dev/${DISK}s1d

	# Mount it
	#

	mkdir ${DEST}
	mount_hammer2 -o local /dev/${DISK}s1d@ROOT ${DEST}

	# Create a separare BUILD PFS for /build
	hammer2 -s ${DEST} -t MASTER pfs-create BUILD
}

create_hier() {
	# Root
	mkdir ${DEST}/boot
	mkdir ${DEST}/tmp
	mkdir ${DEST}/home
	mkdir ${DEST}/root
	mkdir ${DEST}/usr
	mkdir ${DEST}/usr/obj
	mkdir ${DEST}/build
	mkdir ${DEST}/var
	mkdir ${DEST}/var/crash
	mkdir ${DEST}/var/cache
	mkdir ${DEST}/var/spool
	mkdir ${DEST}/var/log
	mkdir ${DEST}/var/tmp

	mount /dev/${DISK}s1a ${DEST}/boot
	mount_hammer2 -o local ${DISK}s1d@BUILD ${DEST}/build

	mkdir ${DEST}/build/usr.obj
	mkdir ${DEST}/build/var.crash
	mkdir ${DEST}/build/var.cache
	mkdir ${DEST}/build/var.spool
	mkdir ${DEST}/build/var.log
	mkdir ${DEST}/build/var.tmp

	mount_null ${DEST}/build/usr.obj ${DEST}/usr/obj
	mount_null ${DEST}/build/var.crash ${DEST}/var/crash
	mount_null ${DEST}/build/var.cache ${DEST}/var/cache
	mount_null ${DEST}/build/var.spool ${DEST}/var/spool
	mount_null ${DEST}/build/var.log ${DEST}/var/log
	mount_null ${DEST}/build/var.tmp ${DEST}/var/tmp

	chmod 1777 ${DEST}/tmp
	chmod 1777 ${DEST}/var/tmp
}

copy_files() {
	cpdup -u -o ${BASE} ${DEST}
	cpdup -u -o ${BASE}/boot ${DEST}/boot
	cpdup -u -o ${BASE}/usr ${DEST}/usr
	cpdup -u -o ${BASE}/var ${DEST}/var
	cpdup -i0 ${BASE}/etc.hdd ${DEST}/etc

	chflags -R nohistory ${DEST}/tmp
	chflags -R nohistory ${DEST}/var/tmp
	chflags -R nohistory ${DEST}/var/crash
	chflags -R nohistory ${DEST}/usr/obj
}

configure_fstab() {
cat > ${DEST}/etc/fstab << EOF
# Device                Mountpoint      FStype  Options         Dump    Pass#
/dev/${DISK}s1d@ROOT    /               hammer2 rw              1       1
/dev/${DISK}s1d@BUILD   /build          hammer2 rw              1       1
/dev/${DISK}s1a         /boot           ufs     rw              1       1
/dev/${DISK}s1b         none            swap    sw              0       0

/build/usr.obj		/usr/obj	null	rw		0	0
/build/var.crash	/var/crash	null	rw		0	0
/build/var.cache	/var/cache	null	rw		0	0
/build/var.spool	/var/spool	null	rw		0	0
/build/var.log		/var/log	null	rw		0	0
/build/var.tmp		/var/tmp	null	rw		0	0
tmpfs			/tmp		tmpfs	rw		0	0
proc                    /proc           procfs  rw              0       0
EOF
}


configure_boot_loader() {
	# Because root is not on the boot partition we have to tell the loader
	# to tell the kernel where root is.
	#
	cat > ${DEST}/boot/loader.conf << EOF
vfs.root.mountfrom="hammer2:${DISK}s1d"
autoboot_delay="1"
EOF
}

configure_rc_conf() {
cat >> ${DEST}/etc/rc.conf << EOF
sshd_enable="YES"
dntpd_enable="YES"
hostname="${HOSTNAME}"
dumpdev="/dev/${DISK}s1b"
ifconfig_${NETIF}="DHCP"
EOF

}

configure_sshd() {
	mkdir -p ${DEST}/root/.ssh
	cp /root/.ssh/authorized_keys ${DEST}/root/.ssh
	mount_devfs ${DEST}/dev
	chroot ${DEST} /usr/bin/ssh-keygen -A
	umount ${DEST}/dev
}

cleanup() {
	rm -R ${DEST}/README* \
		${DEST}/autorun* \
		${DEST}/index.html \
		${DEST}/dflybsd.ico
}

take_snapshot() {
	hammer2 -s BUILD snapshot ${DEST}/build
	hammer2 -s ROOT snapshot ${DEST}
}

dismount() {
	for m in `mount | grep netinst | cut -d ' ' -f 1`; do umount $m; done
	for m in `mount | grep netinst | cut -d ' ' -f 1`; do umount $m; done
	for m in `mount | grep netinst | cut -d ' ' -f 1`; do umount $m; done
}

main() {
	echo "This program formats disk ${DISK}!  Hit ^C now or its gone."
	for _i in 10 9 8 7 6 5 4 3 2 1; do
	    echo -n " ${_i}"
	    sleep 1
	done
	echo ""

	dismount

	mkdir -p ${NETINST_BASE}

	fetch_livecd
	mount_livecd

	partition_disk
	create_fs
	create_hier
	copy_files

	configure_fstab
	configure_boot_loader
	configure_rc_conf
	configure_sshd

	cleanup

	take_snapshot

	dismount
	unmount_livecd
}

main
